package com.atlassian.labs.hipchat.components;

import com.atlassian.confluence.pages.AbstractPage;
import com.google.common.base.Function;
import com.google.common.base.Predicate;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

final class Configurations
{
    private static final String PAGE_CREATED_SUFFIX = ":pc";
    private static final String PAGE_UPDATED_SUFFIX = ":pu";

    static Iterable<String> getRooms(ConfigurationManager configurationManager, AbstractPage page, Predicate<String> predicate, Function<String, String> transform)
    {
        return transform(filter(getRooms(configurationManager, page), predicate), transform);
    }

    static Predicate<String> isBlogPost()
    {
        return new IsBlogPostRoom();
    }

    static Predicate<String> isPageCreated()
    {
        return new IsPageCreatedRoom(PAGE_CREATED_SUFFIX);
    }

    static Predicate<String> isPageUpdated()
    {
        return new IsPageCreatedRoom(PAGE_UPDATED_SUFFIX);
    }

    static Function<String, String> fromPageCreated()
    {
        return new PageCreatedRoomToRoomFunction(PAGE_CREATED_SUFFIX);
    }

    static Function<String, String> fromPageUpdated()
    {
        return new PageCreatedRoomToRoomFunction(PAGE_UPDATED_SUFFIX);
    }

    private static Iterable<String> getRooms(ConfigurationManager configurationManager, AbstractPage page)
    {
        return configurationManager.getHipChatRoomList(page.getSpaceKey());
    }

    private static final class IsBlogPostRoom implements Predicate<String>
    {
        @Override
        public boolean apply(String room)
        {
            return !room.contains(":");
        }
    }

    private static final class IsPageCreatedRoom implements Predicate<String>
    {
        private final String suffix;

        private IsPageCreatedRoom(String suffix)
        {
            this.suffix = suffix;
        }

        @Override
        public boolean apply(String room)
        {
            return room.endsWith(suffix);
        }
    }

    private static final class PageCreatedRoomToRoomFunction implements Function<String, String>
    {
        private final String suffix;

        private PageCreatedRoomToRoomFunction(String suffix)
        {
            this.suffix = suffix;
        }

        @Override
        public String apply(String room)
        {
            return room.split(suffix)[0];
        }
    }
}
