package com.atlassian.labs.hipchat.components;

import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class ConfigurationManager
{
    private final HipChatConfigurationManager hipChatConfigurationManager;

    public ConfigurationManager(HipChatConfigurationManager hipChatConfigurationManager)
    {
        this.hipChatConfigurationManager = checkNotNull(hipChatConfigurationManager);
    }

    public String getHipChatBaseUrl()
    {
        return hipChatConfigurationManager.getApiBaseUrl();
    }

    public String getHipChatAuthToken()
    {
        return hipChatConfigurationManager.getApiToken().getOrElse("");
    }

    public List<String> getHipChatRoomList(String spaceKey)
    {
        return Arrays.asList(StringUtils.split(StringUtils.defaultIfEmpty(hipChatConfigurationManager.get(spaceKey).getOrElse(""), ""), ","));
    }

    public String getHipChatRooms(String spaceKey)
    {
        return hipChatConfigurationManager.get(spaceKey).getOrElse("");
    }

    public void setHipChatBaseUrl(String baseUrl)
    {
        hipChatConfigurationManager.setApiBaseUrl(baseUrl);
    }

    public void setHipChatAuthToken(String authToken)
    {
        hipChatConfigurationManager.setApiToken(authToken);
    }

    public void setNotifyRooms(String spaceKey, String rooms)
    {
        hipChatConfigurationManager.set(spaceKey, rooms);
    }
}