package com.atlassian.labs.hipchat.components;

import com.atlassian.confluence.event.events.content.blogpost.BlogPostCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.TinyUrl;
import com.atlassian.confluence.user.PersonalInformationManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.hipchat.plugins.api.client.ClientError;
import com.atlassian.hipchat.plugins.api.client.HipChatClient;
import com.atlassian.hipchat.plugins.api.client.Message;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.user.User;
import com.atlassian.util.concurrent.Promise;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import static com.atlassian.hipchat.plugins.api.client.Message.BackgroundColor;
import static com.atlassian.hipchat.plugins.api.client.Message.Format;
import static com.atlassian.labs.hipchat.components.Configurations.fromPageCreated;
import static com.atlassian.labs.hipchat.components.Configurations.fromPageUpdated;
import static com.atlassian.labs.hipchat.components.Configurations.getRooms;
import static com.atlassian.labs.hipchat.components.Configurations.isBlogPost;
import static com.atlassian.labs.hipchat.components.Configurations.isPageCreated;
import static com.atlassian.labs.hipchat.components.Configurations.isPageUpdated;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static java.lang.String.format;

public class AnnotatedListener implements DisposableBean, InitializingBean
{
    private static final Logger log = LoggerFactory.getLogger(AnnotatedListener.class);

    private final WebResourceUrlProvider webResourceUrlProvider;
    private final EventPublisher eventPublisher;
    private final ConfigurationManager configurationManager;
    private final PersonalInformationManager personalInformationManager;
    private final UserAccessor userAccessor;
    private final HipChatClient hipChatClient;

    public AnnotatedListener(EventPublisher eventPublisher,
                             ConfigurationManager configurationManager,
                             PersonalInformationManager personalInformationManager,
                             UserAccessor userAccessor,
                             HipChatClient hipChatClient,
                             WebResourceUrlProvider webResourceUrlProvider)
    {
        this.eventPublisher = checkNotNull(eventPublisher);
        this.configurationManager = checkNotNull(configurationManager);
        this.hipChatClient = checkNotNull(hipChatClient);
        this.userAccessor = checkNotNull(userAccessor);
        this.personalInformationManager = checkNotNull(personalInformationManager);
        this.webResourceUrlProvider = checkNotNull(webResourceUrlProvider);
    }

    @EventListener
    public void blogPostCreateEvent(BlogPostCreateEvent event)
    {
        sendMessages(event.getBlogPost(), isBlogPost(), Functions.<String>identity(), "%s&nbsp;%s - new blog post by %s", "blog.png");
    }

    @EventListener
    public void pageCreateEvent(PageCreateEvent event)
    {
        sendMessages(event.getPage(), isPageCreated(), fromPageCreated(), "%s&nbsp;%s - new page created by %s", "page.png");
    }

    @EventListener
    public void pageUpdateEvent(PageUpdateEvent event)
    {
        sendMessages(event.getPage(), isPageUpdated(), fromPageUpdated(), "%s&nbsp;%s - page updated by %s", "page.png");
    }

    private void sendMessages(AbstractPage page, Predicate<String> predicate, Function<String, String> transform, String template, String img)
    {
        final String message = getMessage(page, template, img);
        for (String room : getRooms(configurationManager, page, predicate, transform))
        {
            sendMessage(room, message);
        }
    }

    private String getMessage(AbstractPage page, String template, String img)
    {
        String username = isNullOrEmpty(page.getLastModifierName()) ? page.getCreatorName() : page.getLastModifierName();
        final User user = userAccessor.getUser(username);
        return format(template, imgTag(img), createLink(page), personalSpaceUrl(user));
    }

    private Promise<Either<ClientError, Message.Status>> sendMessage(String r, String message)
    {
        return hipChatClient.rooms().message(r, "Confluence", message, Option.<Format>none(), Option.<BackgroundColor>none(), Option.<Boolean>none());
    }

    private String personalSpaceUrl(User user)
    {
        if (null == user)
        {
            return "";
        }
        return format("<a href=\"%s/%s\">%s</a>", webResourceUrlProvider.getBaseUrl(UrlMode.ABSOLUTE), personalInformationManager.getPersonalInformation(user).getUrlPath(), GeneralUtil.escapeXml(user.getFullName()));
    }

    private String createLink(AbstractPage page)
    {
        return format("<a href=\"%s\"><b>%s</b></a>", GeneralUtil.escapeForHtmlAttribute(tinyLink(page)), GeneralUtil.escapeXMLCharacters(page.getTitle()));
    }

    private String tinyLink(AbstractPage page)
    {
        return webResourceUrlProvider.getBaseUrl(UrlMode.ABSOLUTE) + "/x/" + new TinyUrl(page).getIdentifier();
    }

    private String imgTag(String img)
    {
        return format("<img src=\"%s\" width=16 height=16 />", webResourceUrlProvider.getStaticPluginResourceUrl("com.atlassian.labs.hipchat.confluence-hipchat:image-resources", img, UrlMode.ABSOLUTE));
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        log.debug("Register HipChat event listener");
        eventPublisher.register(this);
    }

    @Override
    public void destroy() throws Exception
    {
        log.debug("Un-register HipChat event listener");
        eventPublisher.unregister(this);
    }
}