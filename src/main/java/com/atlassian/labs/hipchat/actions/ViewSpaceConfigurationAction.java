package com.atlassian.labs.hipchat.actions;

import com.atlassian.confluence.spaces.actions.AbstractSpaceAdminAction;
import com.atlassian.labs.hipchat.components.ConfigurationManager;
import com.opensymphony.xwork.Action;
import org.apache.commons.lang.StringUtils;

public final class ViewSpaceConfigurationAction extends AbstractSpaceAdminAction
{
    private final ConfigurationManager configurationManager;

    private String roomId;
    private boolean successFullUpdate;

    public ViewSpaceConfigurationAction(ConfigurationManager configurationManager)
    {
        this.configurationManager = configurationManager;
    }

    public void setResult(String result) {
        if ("success".equals(result)) {
            successFullUpdate = true;
        }
    }

    @Override
    public String execute()
    {
        setRoomId(configurationManager.getHipChatRooms(key));
        if(StringUtils.isBlank(configurationManager.getHipChatAuthToken())) {
            return Action.INPUT;
        } else {
            return Action.SUCCESS;
        }
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomId() {
        return roomId;
    }

    public boolean isSuccessFullUpdate() {
        return successFullUpdate;
    }
}