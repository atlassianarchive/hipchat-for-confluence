package com.atlassian.labs.hipchat.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.labs.hipchat.components.ConfigurationManager;
import com.atlassian.xwork.RequireSecurityToken;
import com.opensymphony.xwork.Action;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;

public class SaveSpaceConfigurationAction extends ConfluenceActionSupport
{
    private ConfigurationManager configurationManager;
    private SpaceManager spaceManager;

    private String key;
    private String roomId;

    @Override public void validate() {
        super.validate();

        if(StringUtils.isBlank(key) || spaceManager.getSpace(key) == null) {
            addActionError(getText("hipchat.spaceconfig.spacekeyerror"));
        }
    }

    @Override public boolean isPermitted() {
        return spacePermissionManager.hasPermissionForSpace(getRemoteUser(), Arrays.asList(SpacePermission.ADMINISTER_SPACE_PERMISSION), spaceManager.getSpace(key));
    }

    @Override
    @RequireSecurityToken(true)
    public String execute() throws Exception
    {
        configurationManager.setNotifyRooms(key, roomId);
        return Action.SUCCESS;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public void setConfigurationManager(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }
}